<!DOCTYPE html>
<html lang="es">
<head>
	<?php include "./vistas/layouts/head.php" ?>
</head>
<body>
	<?php /* este es el contenedor*/ ?>	
	<?php
	if(isset($_GET['pagina'])) {
// ESTE IF ES MI LISTA BLANCA DE LAS PAGINAS DEL SITIO				
		if( ($_GET['pagina'] == "inicio") || ($_GET['pagina'] == "editor") || ($_GET['pagina'] == "salir") ) {
			include "paginas/".$_GET['pagina'].".php";
		} else{
// SI LA PAGINA NO EXISTE VA A ERROR404					
			include "paginas/error404.php";		
		}
	}else{
// AL INGRESAR AL SITIO VA DIRECTO A INICIO				
		include "paginas/inicio.php";	
	}	
	?>
	<footer class="foot text-center">
		<?php include "./vistas/layouts/footer.php" ?>		
	</footer>
</body>
</html>