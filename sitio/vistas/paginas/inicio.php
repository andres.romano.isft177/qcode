<div class="container py-5">
	<div class="row justify-content-center">
		<div class="d-inline-flex align-items-center">
			<img src="./img/Q-logo-T.png" width="200" height="200">
			<h1 class="display-4 ml-2">Q-Code Project</h1>
		</div>
	</div>	
	<div class="row justify-content-center">
		<p>
			Q-Code es un editor de código on-line con el propósito de practicar HTML, CSS, JavaScript en una misma página. La pantalla se divide en dos para poder ejecutar el código on-line y tener una vista previa de lo escrito, es una alternativa para poder realizar pruebas antes de pasar al código definitivo o simplemente para practicar. Esta configurado con la libreria <a href="https://codemirror.net/doc/manual.html#usage">CodeMirror</a> y lo personalice según la documentación de configuración 
		</p>	
		<p>
			Para aprovechar mejor el espacio de la pantalla es recomendable usar el modo de pantalla completa del navegador, (F11), sobre todo si usas una notebook.
		</p>
		<?php if (isset($_SESSION['name'])): ?>
			<h3>volver al editor</h3>
		<?php else: ?>
		<p>
			Para utilizar el editor solo hace falta escribir un nombre (maximo 20 caracteres), no hace falta registrase ni llenar formularios con datos personales.
		</p>	
		<?php endif ?>
	</div>
	<div class="row justify-content-center">
		<?php if (isset($_SESSION['name'])): ?>
			<a class="editor-icon color-primary-1" href="index.php?pagina=editor"><i class="fas fa-laptop-code"></i></a>
		<?php else: ?>
			<form class="p-5" method="post">				
				<div class="form-group">		
					<div class="input-group">
						<div class="input-group-prepend">
							<span class="input-group-text">
								<i class="fas fa-unlock-alt"></i>
							</span>
							<input type="text" name="sesion" class="form-control" maxlength="20" placeholder="Nombre" title="maximo 20 caracteres" required >
						</div>	
					</div>
				</div>				
				<button type="submit" name="login" class="btn btn-outline-secondary btn-block">ir al editor</button>
			</form>
		<?php endif ?>
	</div>
</div>
<?php
(new ControladorEditor) -> ctrLogin();
