<?php include "./vistas/layouts/navbar.php" ?>
<div class="container-fluid mt-5 pt-3">
    <div class="row">
        <div class="col-lg-6">
            <textarea id="Qcode"></textarea>
        </div>
        <div class="col-lg-6">
            <div class="card full">
                <div class="card-body p-0">                    
                    <iframe id="result" frameborder="0" src="./files/<?php echo $_SESSION['name'] ?>.php" class="full" ></iframe>
                </div>                
            </div>            
        </div>
    </div>  
</div>
<script>
/*
 * Configuracion del Editor:
 * declaro el objeto para el editor y lo configuro segun la documentacion oficial de CodeMirror
 */
var Qcode = CodeMirror.fromTextArea(document.getElementById("Qcode"), {
  mode: "application/x-httpd-php",
  theme: "mbo",
  tabSize: 2,
  sublime: true,
  lineNumbers: true, 
  lineWrapping: true,       
  autoCloseTags: true,
  matchBrackets: true,
  autoCloseBrackets: true,
  keyMap: "sublime",
  extraKeys: {"Ctrl-Space": "autocomplete"},  
  lineWiseCopyCut: true,  
  undoDepth: 200
});
Qcode.setValue('<!DOCTYPE html>\n<html>\n<head>\n<style type="text/css">\n</style>\n</head>\n<body>\n\n</body>\n</html>');
Qcode.setSize("","780");
// con JQuery y Ajax enviamos los datos a un script PHP que graba el archivo
$(document).on('click', '#run', function(e){// al precionar el boton con id="run"
    e.preventDefault();// previene que se recarge la pagina  
    var editorCode = Qcode.getValue();// tomo el valor del text area
    var path = '/qcode/sitio/scripts/'; //path COMPLETO!! donde estaria el archivo file-write.php
    $.ajax({// ahora lo paso por ajax al script php que grabara el archivo        
        url: path + 'file-write.php',
        type: 'POST',
        dataType: 'json',
        data: {"input":editorCode},
    });
  /* esta linea solo hace que se recargue el iframe */
  document.getElementById('result').contentDocument.location.reload(true);  
});
// esto borra el codigo escrito
$(document).on('click', '#clear', function(e){// esta asignado al boton con id="clear"
  e.preventDefault();// previene que se recarge la pagina    
  Qcode.setValue('<!DOCTYPE html>\n<html>\n<head>\n<style type="text/css">\n</style>\n</head>\n<body>\n\n</body>\n</html>');
});
</script>