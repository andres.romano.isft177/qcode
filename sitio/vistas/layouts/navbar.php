<script src="./js/nav.js"></script>
<nav class="navbar navbar-expand navbar-light fixed-top bg-light">
	<div class="d-md-block d-none">
		<a href="index.php?pagina=inicio"><img src="./img/Q-Code-logo-H.png" alt="Logo Q" width="75" class="mr-1"></a>  	
	</div>	
	<ul class="navbar-nav mr-auto">
		<li class="nav-item">
			<a id="download" class="nav-link" title="download"><i class="fas fa-cloud-download-alt"></i></a>
		</li>
		<li class="nav-item">
			<a id="clear" class="nav-link" title="clear"><i class="fas fa-sync-alt"></i></a>
		</li>
		<li class="nav-item">
			<a id="run" class="nav-link" title="run"><i class="fas fa-play"></i></a>
		</li>
		<li>			
			<a id="exit" class="nav-link" title="exit"><i class="fas fa-power-off"></i></a>
		</li>
	</ul>
	<p>Sesión de: <?php echo $_SESSION['name'] ?></p>
</nav>