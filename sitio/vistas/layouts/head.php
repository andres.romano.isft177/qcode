<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<link rel="shortcut icon" href="./img/Q-FV.png" type="image/x-icon">
<link rel="stylesheet" href="./css/style.css">
<link rel="stylesheet" href="./css/bootstrap-4.6.1.min.css">
<script src="./js/c5b742650e.js"></script>
<script src="./js/jquery-3.5.1.min.js"></script>
<link rel="preconnect" href="https://fonts.gstatic.com">
<link href="https://fonts.googleapis.com/css2?family=Montserrat:wght@200&family=Roboto+Slab:wght@300&display=swap" rel="stylesheet">
<meta http-equiv="Expires" content="0">
<meta http-equiv="Last-Modified" content="0">
<meta http-equiv="Cache-Control" content="no-cache, mustrevalidate">
<meta http-equiv="Pragma" content="no-cache">
<!-- LIBRERIAS DE CODEMIRROR -->
<link rel="stylesheet" href="./codemirror/lib/codemirror.css"> 
<link rel="stylesheet" type="text/css" href="./codemirror/theme/mbo.css">
<link rel="stylesheet" type="text/css" href="./codemirror/theme/neo.css">
<link rel="stylesheet" type="text/css" href="./codemirror/addon/hint/show-hint.css">	
<script src="./codemirror/lib/codemirror.js"></script>
<script src="./codemirror/mode/htmlmixed/htmlmixed.js"></script>
<script src="./codemirror/mode/htmlembedded/htmlembedded.js"></script>
<script src="./codemirror/mode/xml/xml.js"></script>  
<script src="./codemirror/mode/javascript/javascript.js"></script>
<script src="./codemirror/mode/css/css.js"></script>  
<script src='./codemirror/mode/php/php.js'></script>
<script src="./codemirror/mode/clike/clike.js"></script>
<script src='./codemirror/keymap/sublime.js'></script>  
<script src='./codemirror/addon/selection/active-line.js'></script>  
<script src='./codemirror/addon/edit/closetag.js'></script>
<script src='./codemirror/addon/edit/closebrackets.js'></script>
<script src='./codemirror/addon/edit/matchbrackets.js'></script>
<script src='./codemirror/addon/hint/show-hint.js'></script>
<script src='./codemirror/addon/hint/css-hint.js'></script>
<script src='./codemirror/addon/hint/html-hint.js'></script>
<script src='./codemirror/addon/hint/xml-hint.js'></script>
<script src='./codemirror/addon/hint/javascript-hint.js'></script>
<title>Q CODE</title>