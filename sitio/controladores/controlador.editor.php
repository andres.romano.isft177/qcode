<?php
class ControladorEditor {	
	public function ctrLogin() {
		if (isset($_POST['login'])) {
			if ( (isset($_POST['sesion'])) && (!empty($_POST['sesion'])) && (strlen($_POST['sesion']) <= 20) ) {
				$_SESSION['name'] = $_POST['sesion'];
				$path = './files/';
				touch($path.$_SESSION['name'].".php");
				$file = $path.$_SESSION['name'].".php";
				chmod($file, 0755);
				echo "<script>
				if ( window.history.replaceState ) {
						window.history.replaceState(null, null, window.location.href);
					}
				window.location = 'index.php?pagina=editor';
				</script>";
			} else {
				echo "<script>
				alert('escribe un nombre, de maximo 20 caracteres');
				</script>";
			}
		} 
	}

}