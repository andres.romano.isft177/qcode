<?php
// EL INDEX: Salida de las vistas al usuario y se envian las distintas acciones del usuario al controlador
/*
error_reporting(E_ALL);
ini_set('display_errors', true);
ini_set('html_errors', false);
*/
require_once "./controladores/controlador.home.php";
require_once "./controladores/controlador.editor.php";

/*
//Probar la conexion con la DB
require_once "modelos/conexion.php";
$conexion = Conexion::conectar();
echo '<pre>'; print_r($conexion); echo '</pre>';
*/
// invoco la plantilla principal
$home = new ControladorHome;
$home -> ctrTraerHome();
